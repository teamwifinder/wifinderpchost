package lib.simulation;

/**
 * This class represents a position in 3d cartesion world space.
 * @author Eileen Neumann
 * @version 1.0
 */
public class WorldPosition {

    /** x position in world space */
    public final float x;
    /** y position in world space */
    public final float y;
    /** z position in world space */
    public final float z;
    
    /**
     * Create a new position in world space
     * @param x x component
     * @param y y component
     * @param z z component
     */
    public WorldPosition(final float x, final float y, final float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
}
