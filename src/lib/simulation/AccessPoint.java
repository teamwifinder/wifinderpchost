package lib.simulation;

/**
 * This class represents a access point in the context of an signal strength simulation.
 * An access point is modeled by  having a position in world space and a emitted signal strength;
 * @author Eileen Neumann
 * @version 1.0
 */
public class AccessPoint {

    /** the ssid of the access point */
    public final String ssid;
    /** emitted signal strength */
    public float strength;
    /** x world coordinate */
    public float x;
    /** y world coordinate */
    public float y;
    /** z world coordinate */
    public float z;
    
    /** 
     * Create a new access point simulation object 
     * @param x x Cartesian position in world space
     * @param y y Cartesian position in world space
     * @param z z Cartesian position in world space
     * @param strength emitted signal strength
     * @param ssid the ssid of the access point
     */
    public AccessPoint(final float x, final float y, final float z, final float strength, final String ssid) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.strength = strength;
        this.ssid = ssid;
    }
    
}
