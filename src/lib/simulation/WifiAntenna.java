package lib.simulation;

/**
 * This class represents a wifi antenna.
 * A wifi probe is used the evaluate the signal strength of an access point.
 * @author Eileen Neumann
 * @version 1.0
 */
public class WifiAntenna {

    /** Gain of the receiver antenna */
    public final float gain;
    
    /**
     * Create a new wifi antenna
     * @param gain the gain of the antenna
     */
    public WifiAntenna(final float gain) {
        this.gain = gain;
    }
    
}
