package lib.simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a simulated wifi environment.
 * @author Eileen Neumann
 * @version 1.0
 */
public class WifiEnvironment {

    /** a list of access points in the world */
    public List<AccessPoint> accessPoints;
 
    /**
     * Create a new empty wifi environment
     */
    public WifiEnvironment() {
        this.accessPoints = new LinkedList<>();
    }
    
    /**
     * Return the signal strength of the given ssid in this wifi environment.
     * The signal strength is dependent on the position and the antenna used, as well as
     * the strength of the emitter.
     * @param ssid the ssid of the access point to track
     * @param p the position to probe the field strength at
     * @param a the antenna to use for the probe
     * @return the rssi
     */
    public float getRssiAt(final String ssid, final WorldPosition p, final WifiAntenna a) {
        return 0.0f;
    }
    
}
